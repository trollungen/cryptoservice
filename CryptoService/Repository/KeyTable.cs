﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoService
{
    [Table("KeyTable")]
    public class KeyTable
    {
        [Key]
        public Guid Id { get; set; }
        public string Key { get; set; }
    }
}
