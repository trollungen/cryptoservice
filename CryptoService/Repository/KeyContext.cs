﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoService
{
    public class KeyContext : DbContext
    {
        public KeyContext()
            : base()
        {

        }

        public DbSet<KeyTable> Keys { get; set; }
    }
}
