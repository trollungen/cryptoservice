﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CryptoService
{
    public class KeyStorage
    {
        public void AddToStorage(Guid keyid, string privateKey)
        {
            using (var db = new KeyContext())
            {
                var keycollection = new KeyTable() 
                { 
                    Id = keyid, 
                    Key = privateKey,
                };
                db.Keys.Add(keycollection);
                db.SaveChanges();
            }
        }

        public void DeleteKeys(Guid keyid)
        {
            using (var db = new KeyContext())
            {
                var keys = db.Keys.FirstOrDefault(i => i.Id == keyid);

                if (keys != null)
                {
                    db.Keys.Remove(keys);
                    db.SaveChanges();
                }
            }
        }

        public string GetKeyToDecrypt(Guid keyid)
        {
            using (var db = new KeyContext())
            {
                var keys = db.Keys.FirstOrDefault(i => i.Id == keyid);

                if (keys != null)
                    return keys.Key;
            }
            return null;
        }
    }
}
