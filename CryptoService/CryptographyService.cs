﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CryptoService
{
    public class CryptographyService
    {
        public string GetDataForEncryption()
        {
            PublicKeyResponse response = new PublicKeyResponse();
            KeyStorage repository = new KeyStorage();

            try
            {
                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
                var privateKey = rsa.ToXmlString(true);
                var publicKey = rsa.ToXmlString(false);

                response.KeyId = Guid.NewGuid();
                response.PublicKey = publicKey;
                repository.AddToStorage(response.KeyId, privateKey);
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }
            return JsonConvert.SerializeObject(response);
        }

        public string EncryptMessageAES(string message, string publickey)
        {
            EncryptedMessageResponse response = new EncryptedMessageResponse();
            try
            {
                var cipher = CreateCipher(publickey);
                response.InitialisationVector = Convert.ToBase64String(cipher.IV);
                var encrypt = cipher.CreateEncryptor();
                byte[] messageInByte = Encoding.UTF8.GetBytes(message);
                byte[] cipherInByte = encrypt.TransformFinalBlock(messageInByte, 0, messageInByte.Length);
                response.Ciphertext = Convert.ToBase64String(cipherInByte);
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }

            return JsonConvert.SerializeObject(response);
        }

        public string EncryptPublicKey(string AESpublickey, string recieverPublicKey)
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            rsa.FromXmlString(recieverPublicKey);

            byte[] data = Encoding.UTF8.GetBytes(AESpublickey);
            byte[] cipherText = rsa.Encrypt(data, false);
            return Convert.ToBase64String(cipherText);
        }

        public string EncryptFinalMessage(Guid keyid, string encryptedPublicKey, string ciphertext, string InitialisationVector)
        {
            var cryptedMessage = new CryptoMessageResult()
            {
                MessageEncryption = new EncryptedMessageResponse() { Ciphertext = ciphertext, InitialisationVector = InitialisationVector },
                SenderEncryptedPublicKey = encryptedPublicKey,
                RecieverKeyId = keyid,
            };

            return JsonConvert.SerializeObject(cryptedMessage);
        }

        public string DecryptMessage(string messageAsJson)
        {
            try
            {
                CryptoMessageResult message = JsonConvert.DeserializeObject<CryptoMessageResult>(messageAsJson);
                string keypairInStore = GetKeyPairFromRepository(message.RecieverKeyId);
                if (keypairInStore == null)
                    return "message already encrypted";

                string senderPublicKey = DecryptSenderPublicKey(message.SenderEncryptedPublicKey, keypairInStore);

                var plainText = DecryptMessageToPlainText(senderPublicKey, message.MessageEncryption.InitialisationVector, message.MessageEncryption.Ciphertext);
                return plainText;
            }
            catch (Exception)
            {
                return "The message was not able to decrypt";
            }
        }

        private string GetKeyPairFromRepository(Guid keyid)
        {
            KeyStorage repository = new KeyStorage();
            var privateKey = repository.GetKeyToDecrypt(keyid);
            if (privateKey != null)
            {
                repository.DeleteKeys(keyid);
                return privateKey;
            }
            return null;
        }

        private string DecryptSenderPublicKey(string encryptedPublicKey, string keyPair)
        {
            byte[] publicKey = Convert.FromBase64String(encryptedPublicKey);
            string completedKeyPair = keyPair;

            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            rsa.FromXmlString(completedKeyPair);
            byte[] publicKeyInByte = rsa.Decrypt(publicKey, false);
            var test = Encoding.UTF8.GetString(publicKeyInByte);
            return test;
        }

        private string DecryptMessageToPlainText(string senderPublicKey, string initializationVector, string cipherText)
        {
            var cipher = CreateCipher(senderPublicKey, initializationVector);
            var decrypt = cipher.CreateDecryptor();
            byte[] cipherInByte = Convert.FromBase64String(cipherText);
            byte[] messageInByte = decrypt.TransformFinalBlock(cipherInByte, 0, cipherInByte.Length);
            return Encoding.UTF8.GetString(messageInByte);
        }

        private RijndaelManaged CreateCipher(string publicKey, string iv = null)
        {
            RijndaelManaged cipher = new RijndaelManaged();
            cipher.KeySize = 128;
            if (iv != null)
                cipher.IV = Convert.FromBase64String(iv);

            cipher.BlockSize = 128;
            cipher.Padding = PaddingMode.ISO10126;
            cipher.Mode = CipherMode.CBC;
            byte[] pk = HexToByteArray(publicKey);
            cipher.Key = pk;
            return cipher;

        }

        private byte[] HexToByteArray(string publicKey)
        {
            if (0 != (publicKey.Length % 2))
            {
                throw new ApplicationException("Publickey must be multiple of 2 in length");
            }

            int byteCount = publicKey.Length / 2;
            byte[] byteValues = new byte[byteCount];
            for (int i = 0; i < byteCount; i++)
            {
                byteValues[i] = Convert.ToByte(publicKey.Substring(i * 2, 2), 16);
            }

            return byteValues;
        }
    }
}
