﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoService
{
    public class EncryptedMessageResponse : ResponseBase
    {
        public string Ciphertext { get; set; }
        public string InitialisationVector { get; set; }
    }
}
