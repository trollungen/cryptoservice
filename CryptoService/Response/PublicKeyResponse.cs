﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CryptoService
{
    public class PublicKeyResponse : ResponseBase
    {
        public Guid KeyId { get; set; }
        public String PublicKey { get; set; }
    }
}
