﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoService
{
    public class ResponseBase
    {
        public ResponseBase()
        {
            this.Exception = null;
        }

        public Exception Exception { get; set; }
    }
}
