﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoService
{
    public class CryptoMessageResult
    {
        public EncryptedMessageResponse MessageEncryption { get; set; }
        public string SenderEncryptedPublicKey { get; set; }
        public Guid RecieverKeyId { get; set; }

    }
}
