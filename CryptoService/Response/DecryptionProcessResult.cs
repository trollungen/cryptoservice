﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoService
{
    public class DecryptionProcessResult : ResponseBase
    {
        public string MessageResult { get; set; }
    }
}
