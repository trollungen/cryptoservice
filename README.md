Crypto Service

Klass biblioteket krypterar data som skickas mellan två användare.

Biblioteket har fem methoder

**GetDataForEncryption**
Denna metoden skapar upp data och returnerar en publik nyckel och en keyid, i form av en Jsonstring.

*returnerar exempel: {"KeyId":"a404d243-0add-4766-b3f2-7b2e45c32a68","PublicKey":"<RSAKeyValue><Modulus>uxVV1/fxani3qy9mtau9WXrRZqNPdyhWIsszMOHwsDpemyGFkJRicLRFLZSpLmfOqoW44kuiycCFeQknMiXmnujgtMVsRXz9gEcpt+eo3yjULIT7Sx9NP8q68EeN4SmFRZSRL57SATYyQZfrWttLRJaQZqJl3ENnmTwSnflDBcU=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>","Exception":null}*


**EncryptMessageAES**
Med denna metod krypterar du ditt meddelande med hjälp av din publika nyckel, som är en 152 bit key

*returnerar exempel: {"Ciphertext":"I1PPQcGmnEHohLrfuOCweg==","InitialisationVector":"oeLrJjqedmnF/qyyPcXf7A==","Exception":null}*


**EncryptPublicKey**
Denna metoden krypterar din nyckel med hjälp av den publika nyckeln du fick av metoden 'GetDataForEncryption'.

*returnerar exempel: Pk6EjTokbgLuZO+TFY9mQbjfnssRdKDH5Lu0T+FowlpFIxCKYemE+Tn4np0XicJIctkBbu3xyINlygovaJsr9rmehuWUN4ckpA5jNz5ERIW7sSPSkNvm4Lw7UlaKlYpFE05DMYUChqIchyDjlYAR6Xrf1u6Cy3L+Cit/UJ5KwHU=*


**EncryptFinalMessage**
Här skickar du in din krypterade publika nyckel, keyid, samt datan du fick i retur av 'EncryptMessageAES' (ciphertext och InitialisationVector)
Tillbaka får du det krypterade meddelandet i en jsonstring.


*returnerar exempel: {"MessageEncryption":{"Ciphertext":"I1PPQcGmnEHohLrfuOCweg==","InitialisationVector":"oeLrJjqedmnF/qyyPcXf7A==","Exception":null},"SenderEncryptedPublicKey":"fYPjsZBGRJloDlVSgUTHPfQjeJo251FyWAEo5apZ9JvKIRJQOvM+3xs5fexknNNmXz78ATTzhWMWB5dkU9Ks48Fo7Pf1qVB9vU2UM9cjP890KJg4bF5+4BePD/IhOZe534mZTn/ZCZ/u1U/r8y/F2X8vqMQ2JrIYnjsFJVkcasI=","RecieverKeyId":"a404d243-0add-4766-b3f2-7b2e45c32a68"}*

**DecryptMessage**
För att dekryptera meddelandet skickar du in hela jsonstringen som du fick i retur av 'EncryptFinalMessage'

returnerar ditt hemliga meddelande i klartext