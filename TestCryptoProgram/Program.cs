﻿using CryptoService;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace TestCryptoProgram
{
    class Program
    {
        public static void Main()
        {
            RunCryptoService();

            Console.ReadLine();
        }

        private static void RunCryptoService()
        {
            Console.Write("Meddelande: ");
            var msg = Console.ReadLine();
            var cryptoservice = new CryptographyService();
            var publicKey = TryToGenerateKey();

            String encryptionData = cryptoservice.GetDataForEncryption();
            PublicKeyResponse recieverData = JsonConvert.DeserializeObject<PublicKeyResponse>(encryptionData);
            Console.WriteLine(encryptionData);

            String encryptedMessage = cryptoservice.EncryptMessageAES(msg, publicKey);
            EncryptedMessageResponse encryptedMessageData = JsonConvert.DeserializeObject<EncryptedMessageResponse>(encryptedMessage);
            Console.WriteLine(encryptedMessage);

            String myEncryptPublicKey = cryptoservice.EncryptPublicKey(publicKey, recieverData.PublicKey);
            Console.WriteLine(myEncryptPublicKey);

            var cryptmsg = cryptoservice.EncryptFinalMessage(recieverData.KeyId, myEncryptPublicKey, encryptedMessageData.Ciphertext, encryptedMessageData.InitialisationVector);

            Console.WriteLine(cryptmsg);

            var decryptedMessage = cryptoservice.DecryptMessage(cryptmsg);

            Console.WriteLine(decryptedMessage);
        }


        public static string TryToGenerateKey()
        {
            var random = new Random();
            var chars = "1346798520";
            var result = new string(Enumerable.Repeat(chars, 32).Select(s => s[random.Next(s.Length)]).ToArray());
            return result;
        }
    }
}
